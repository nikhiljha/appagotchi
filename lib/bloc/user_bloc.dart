import 'dart:async';
import 'dart:convert';
import 'package:appagotchi/bloc/user.dart';
import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './bloc.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  @override
  UserState get initialState => InitialUserState();

  void saveDbUser(User user) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString("user", json.encode(user.toJson()));
  }

  Future<User> getDbUser() async {
    final prefs = await SharedPreferences.getInstance();
    String jsonString = prefs.getString("user");
    if (jsonString == null || jsonString == "") {
      return fakeUser;
    }
    return User.fromJson(json.decode(prefs.getString("user")));
  }

  final User fakeUser = User(
    ip: "10.0.0.2",
  );

  @override
  Stream<UserState> mapEventToState(
    UserEvent event,
  ) async* {
    if (event is InitUser) {
      final user = await getDbUser();
      yield LoadedUserState(user);
    }

    if (event is EditUser) {
      saveDbUser(event.changed);
      yield LoadedUserState(event.changed);
    }
  }
}

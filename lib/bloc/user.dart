class User {
  String ip;

  User({this.ip});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      ip: json['ip'],
    );
  }

  Map toJson() {
    return {
      "ip": this.ip,
    };
  }
}

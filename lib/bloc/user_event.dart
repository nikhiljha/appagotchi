import 'package:appagotchi/bloc/user.dart';

abstract class UserEvent {
  const UserEvent();
}

class InitUser extends UserEvent {
  @override
  List<Object> get props => null;
}

class EditUser extends UserEvent {
  final User changed;

  const EditUser(this.changed);

  @override
  List<Object> get props => [changed];
}

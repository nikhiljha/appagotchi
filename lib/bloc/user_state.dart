import 'package:appagotchi/bloc/user.dart';

abstract class UserState {
  const UserState();
}

class InitialUserState extends UserState {
  @override
  List<Object> get props => null;
}

class LoadingUserState extends UserState {
  @override
  List<Object> get props => null;
}

class LoadedUserState extends UserState {
  final User user;

  const LoadedUserState(this.user);

  @override
  List<Object> get props => [user];
}

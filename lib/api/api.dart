import 'dart:convert';

import 'package:appagotchi/api/message.dart';
import 'package:appagotchi/bloc/user.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class PwnAPI {
  static getIP() async {
    final prefs = await SharedPreferences.getInstance();
    String jsonString = prefs.getString("user");
    if (jsonString == null || jsonString == "") {
      return "10.0.0.2";
    }
    return User.fromJson(json.decode(prefs.getString("user"))).ip;
  }

  static Future<List<Message>> getAllMessages() async {
    var ip = await getIP();
    var res = await (http.get(Uri.encodeFull("http://$ip:8666/api/v1/inbox")));
    var data = json.decode(res.body);
    List messages = data["messages"] as List;
    var getted = messages
        .map<Future<Message>>((json) async => await getMessage(json["id"]))
        .toList();
    return await Future.wait(getted);
  }

  static Future<Message> getMessage(int id) async {
    var ip = await getIP();
    var res =
        await (http.get(Uri.encodeFull("http://$ip:8666/api/v1/inbox/$id")));
    var data = json.decode(res.body);
    return Message.fromJson(data);
  }

  static sendMessage(String message, String recipient) async {
    var ip = await getIP();
    var res = await (http.post(
      Uri.encodeFull("http://$ip:8666/api/v1/unit/$recipient/inbox"),
      body: message,
    ));
    return res.statusCode;
  }
}

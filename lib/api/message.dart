class Message {
  DateTime createdAt;
  DateTime deletedAt;
  int id;
  DateTime seenAt;
  String sender;
  String senderName;
  DateTime updatedAt;
  String data;
  String signature;

  Message({
    this.createdAt,
    this.deletedAt,
    this.id,
    this.seenAt,
    this.sender,
    this.senderName,
    this.updatedAt,
    this.data,
    this.signature,
  });

  static DateTime safeDateParse(String strDate) {
    if (strDate == null) {
      return null;
    }
    return DateTime.parse(strDate);
  }

  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
      createdAt: safeDateParse(json["created_at"]),
      deletedAt: safeDateParse(json["deleted_at"]),
      id: json["id"],
      seenAt: safeDateParse(json["seen_at"]),
      sender: json["sender"],
      senderName: json["sender_name"],
      updatedAt: safeDateParse(json["updated_at"]),
      data: json["data"],
      signature: json["signature"],
    );
  }
}

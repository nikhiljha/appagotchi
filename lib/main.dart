import 'package:appagotchi/bloc/bloc.dart';
import 'package:appagotchi/home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() => runApp(Appagotchi());

class Appagotchi extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: <BlocProvider>[
        BlocProvider<UserBloc>(
          builder: (context) => UserBloc(),
        ),
      ],
      child: MaterialApp(
        title: 'Appagotchi',
        theme: ThemeData(
          primarySwatch: Colors.amber,
          brightness: Brightness.light,
        ),
        darkTheme: ThemeData(
          primarySwatch: Colors.amber,
          brightness: Brightness.dark,
        ),
        home: HomePage(),
      ),
    );
  }
}

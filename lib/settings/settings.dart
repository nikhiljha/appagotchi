import 'package:appagotchi/bloc/bloc.dart';
import 'package:appagotchi/bloc/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => new _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  TextEditingController _ip = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
      ),
      body: BlocBuilder(
          bloc: BlocProvider.of<UserBloc>(context),
          builder: (context, state) {
            if (state is LoadedUserState) {
              _ip.text = state.user.ip;
              return ListView(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                children: <Widget>[
                  SizedBox(height: 10),
                  TextField(
                    controller: _ip,
                    decoration: InputDecoration(
                      labelText: "IP Address",
                      labelStyle: TextStyle(color: Colors.grey),
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey),
                          borderRadius: BorderRadius.circular(25.0)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey),
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                  ),
                  RaisedButton(
                    child: Text("Save"),
                    splashColor: Colors.grey[800],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    onPressed: () async {
                      User current =
                          await BlocProvider.of<UserBloc>(context).getDbUser();
                      current.ip = _ip.text;
                      BlocProvider.of<UserBloc>(context).add(
                        EditUser(current),
                      );
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            } else {
              BlocProvider.of<UserBloc>(context).add(InitUser());
              return Center(
                child: Text("User not loaded"),
              );
            }
          }),
    );
  }
}

import 'dart:convert';

import 'package:appagotchi/api/api.dart';
import 'package:appagotchi/api/message.dart';
import 'package:appagotchi/bloc/bloc.dart';
import 'package:appagotchi/home/compose.dart';
import 'package:appagotchi/settings/settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  HomePage() : super();

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Appagotchi"),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          children: <Widget>[
            UserAccountsDrawerHeader(
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.white,
                child: Text(
                  "??",
                  style: TextStyle(fontSize: 40.0),
                ),
              ),
              accountEmail: Text("idstringtaptocopy"),
              accountName: Text("Your Pwnagotchi"),
            ),
            BlocBuilder(
              bloc: BlocProvider.of<UserBloc>(context),
              builder: (context, state) {
                if (state is InitialUserState) {
                  BlocProvider.of<UserBloc>(context).add(InitUser());
                }
                return SizedBox(
                  height: 0,
                );
              },
            ),
            ListTile(
              title: Text("Settings"),
              leading: Icon(Icons.settings),
              trailing: Icon(Icons.arrow_forward),
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => SettingsPage(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      body: FutureBuilder<List<Message>>(
          future: PwnAPI.getAllMessages(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (snapshot.data.length == 0) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "(ب__ب)",
                      style: TextStyle(fontSize: 64.0, color: Colors.grey),
                    ),
                    Text(
                      "no messages",
                      style: Theme.of(context).textTheme.subtitle,
                    ),
                  ],
                ),
              );
            }
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  leading: CircleAvatar(
                    child: Text(
                      snapshot.data[index].senderName.substring(0, 1),
                    ),
                  ),
                  title: Text(
                      snapshot.data[index].sender.substring(0, 20) + "..."),
                  subtitle: Text(
                    utf8.decode(
                      base64.decode(snapshot.data[index].data),
                    ),
                  ),
                );
              },
            );
          }),
      floatingActionButton: FloatingActionButton(
        tooltip: 'New Message',
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => ComposePage(),
            ),
          );
        },
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

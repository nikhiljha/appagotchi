import 'package:appagotchi/api/api.dart';
import 'package:flutter/material.dart';

class ComposePage extends StatefulWidget {
  @override
  _ComposePageState createState() => _ComposePageState();
}

class _ComposePageState extends State<ComposePage> {
  TextEditingController _to = new TextEditingController();
  TextEditingController _message = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Compose"),
      ),
      body: ListView(
        children: <Widget>[
          TextField(
            controller: _to,
            decoration: InputDecoration(
              labelText: "Recipient",
              labelStyle: TextStyle(color: Colors.grey),
              contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(25.0)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(25.0)),
            ),
          ),
          TextField(
            controller: _message,
            keyboardType: TextInputType.multiline,
            decoration: InputDecoration(
              labelText: "Message",
              labelStyle: TextStyle(color: Colors.grey),
              contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(25.0)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(25.0)),
            ),
          ),
          RaisedButton(
            child: Text("Send"),
            splashColor: Colors.grey[800],
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            onPressed: () async {
              await PwnAPI.sendMessage(_message.text, _to.text);
              // TODO: Make sure this is 200.
              Navigator.of(context).pop();
            },
          )
        ],
      ),
    );
  }
}
